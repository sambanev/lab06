from binaryConverter import binToDecList

def test_generic_case():
    binary_input = [1, 1, 0, 0, 1]
    expected_decimal_output = 25
    assert binToDecList(binary_input) == expected_decimal_output
